#!/usr/bin/python3
#
# GCompris - wordsgameToPo.py
#
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
#
#   SPDX-License-Identifier: GPL-3.0-or-later

import sys
import json
import os
import datetime
import polib
import urllib
from urllib.parse import quote

if(len(sys.argv) < 2):
    print("Usage: wordsgameToPo.py output.po [content-fr.json]")
    print("  The optional argument is used to backport manually created json")
    sys.exit(1)

def loadManualFile(manual):
    json_data = open(manual)
    manualData = json.load(json_data)
    json_data.close()
    return manualData

def getManualTranslation(manualData, lev):
    if not manualData:
        return ""
    try:
        words = []
        for level in manualData['levels']:
            if (int(level['level']) == lev):
                for word in level['words']:
                    words.append(word)
        return ",".join(words)
    except:
        return ""

def getManualSublevels(manualData, lev):
    if not manualData:
        return ""
    try:
        words = []
        for level in manualData['levels']:
            if (int(level['level']) == lev):
                return level['sublevels']
        return ",".join(words)
    except:
        return ""

def getManualDescription(manualData):
    if not manualData:
        return ""
    try:
        return manualData["description"]
    except:
        return ""

manualData = None
if(len(sys.argv) == 3):
    manualData = loadManualFile(sys.argv[2])

displayInConsole = False

# Header
po = polib.POFile()
po.metadata = {
    'Project-Id-Version': 'gcompris_qt\\n',
    'Report-Msgid-Bugs-To': 'https://bugs.kde.org/enter_bug.cgi?product=gcompris',
    'POT-Creation-Date': '2023-12-30 00:00+0000',
    'PO-Revision-Date': '2023-12-30 00:00+0000',
    'Last-Translator': 'FULL NAME <EMAIL@ADDRESS>\n',
    'Language-Team': 'LANGUAGE <kde-i18n-doc@kde.org>\n',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=utf-8',
    'Content-Transfer-Encoding': '8bit',
}

for level in range(1, 26):
    match level:
        case 1:
            constraint = 'length <4 and very easy to type'
        case 2:
            constraint = 'length 4 or <4 and not so easy'
        case 3:
            constraint = 'length 5'
        case 4:
            constraint = 'length 6'
        case 5:
            constraint = 'length >6'
        case _:
            constraint = 'your own additional levels/rules'
    msgid_ = 'Translators: Don\'t translate this text. This is the vocabulary for level ' + str(level) + ':\nYou should fill in 100 but at least 15 comma separated words with ' + constraint + ' in your language.\nThis rule is non-binding: You can translate less or more levels and so provide special rules for your language. If you want only the 5 recommended levels just translate the last one with ".". Scripty will ignore it an you don\'t have one untranslated. I provide 25 (= 20 additional) levels because "ml" does so. Feel free how you handle it.'
    msgidlev_ = 'Translators: With this you control how many sublevels (tasks) for level ' + str(level) + ' our youngest users will have. Default is 15. Keep in mind the vocabulary for this level must have at least this count of words. If you don\'t provide a level translate it with "0" and scripty will ignore it. There will also be an adaption if you say 15 here und you have only 14 translated. So things shouldn\'t break ...'
    msgstr_ = getManualTranslation(manualData, level)
    manlev = getManualSublevels(manualData, level)
    msgstrs = msgstr_.count(",") + 1
    if manlev != '':
        if msgstrs < int(manlev):
            manlev = msgstrs
    msgctx_ = ""
    comment_ = "wordsgame_Level_" + str(level)
    if displayInConsole:
        print("#. " + comment_ + "_numSubLevels")
        print('msgctxt "' + msgctx_ + '"')
        print('msgid "' + msgidlev_ + '"')
        print('msgstr "' + manlev + '"')
        print("")
        print("#. " + comment_ + "_vocabulary")
        print('msgctxt "' + msgctx_ + '"')
        print('msgid "' + msgid_ + '"')
        print('msgstr "' + msgstr_ + '"')
        print("")

    entry = polib.POEntry(
        msgid = msgidlev_,
        msgstr = manlev,
        msgctxt = msgctx_,
        comment = comment_ + "_numSubLevels",
    )
    po.append(entry)

    entry = polib.POEntry(
        msgid = msgid_,
        msgstr = msgstr_,
        msgctxt = msgctx_,
        comment = comment_ + "_vocabulary",
    )
    po.append(entry)

entry = polib.POEntry(
   msgid = 'Translators this is the description. Type here your language in english please: e.g. "Default German", "Default French", etc.',
   msgstr = getManualDescription(manualData),
   msgctxt = msgctx_,
   comment = "wordsgame_translation_description"
)
po.append(entry)

po.save(sys.argv[1])
