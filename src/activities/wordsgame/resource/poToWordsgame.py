#!/usr/bin/python3
#
# GCompris - poToWordsgame.py
#
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
#
#   SPDX-License-Identifier: GPL-3.0-or-later

import json
import polib
import sys

if len(sys.argv) != 4:
    print("Usage: po2Wordsgame.py po_file json_file lang")
    sys.exit(1)

poFile = polib.pofile(sys.argv[1], encoding='utf-8')
jsonFile = sys.argv[2]
lang = sys.argv[3]
data = {}
stream = {}
levels = []
entries = 0

# Get all entries since we can also work with fuzzy and untranslated here
valid_entries = [e for e in poFile if not e.obsolete]
for entry in valid_entries:
    data[entries] = entry.msgstr
    entries += 1

for i in range(0, entries - 1, 2):
    level = {}
    wordstr = data[i + 1]
    words = wordstr.count(",") + 1
    if wordstr == "" or wordstr == "." or words < 5:
        continue
    forced = data[i]
    sublevels = words
    if sublevels > 15:
        sublevels = 15
    if forced.isdigit():
        sublevels = int(forced)
        if sublevels == 0:
            continue
    if sublevels > words:
        sublevels = words
    level["level"] = str(i // 2 + 1)
    level["sublevels"] = str(sublevels)
    level["words"] = wordstr.split(",")
    levels.append(level)

stream["levels"] = levels
stream["name"] = "default-" + lang
stream["locale"] = lang
stream["description"] = data[entries - 1]

with open(jsonFile, "w", encoding='utf-8') as data_file:
    json.dump(stream, data_file, indent=3, sort_keys=False, ensure_ascii=False)
    data_file.write("\n")  # Add missing new line at end of file
