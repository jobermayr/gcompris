#!/usr/bin/env bash
#=============================================================================
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#=============================================================================
# Copied from https://cgit.kde.org/plasma-browser-integration.git/tree/StaticMessages.sh

# We fill in the en "translations" manually. We extract this to the KDE system as pot as normal, then populate the other json files

# The name of catalog we create (without the.pot extension), sourced from the scripty scripts
FILENAME="gcompris_wordsgame"

function export_pot_file # First parameter will be the path of the pot file we have to create, includes $FILENAME
{
    potfile=$1
    python3 ./resource/wordsgameToPo.py $potfile
}

function import_po_files # First parameter will be a path that will contain several .po files with the format LANG.po
{
    podir=$1
    for file in `ls $podir`
    do
        lang=${file%.po} #remove .po from end of file
        python3 ./resource/poToWordsgame.py $podir/$file ./resource/default-$lang.json $lang
    done
}
